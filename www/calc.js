'use strict';

/*  Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones.

        - El programa debe recibir dos números (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta, multiplicación
          o división).

        - Opcional: agrega una operación que permita elevar n1 a la potencia n2.*/


function calculadora() {

  let n1 = parseInt(prompt('Dime un número'));
  let n2 = parseInt(prompt('Dime otro número'));
  let nX = (prompt('¿Qué operación quieres realizar?'));

  if (nX === 'sumar') {
    return n1 + n2;
  }
  if (nX === 'restar') {
    return n1 - n2;
  }
  if (nX === 'multiplicar') {
    return n1 * n2;
  }
  if (nX === 'dividir') {
    return n1 / n2;
  }
  if (nX === 'potencia') {
    return n1 ** n2;
  }
}
console.log(calculadora());

